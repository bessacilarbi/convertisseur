const eurDOLInput = document.getElementById('input-eurDOL')
const DOLeurInput = document.getElementById('input-DOLeur')

  // de la valeur du champ
eurDOLInput.addEventListener('input', () => {
  // la valeur saisie par l'utilisateur
  const eur = eurDOLInput.value

  // Convertir la valeur en nombre
  const eurNombre = parseFloat(eur)

  // le taux
  const taux = DOLeurInput.getAttribute('data-taux')

  // Convertir le taux en nombre
  const tauxNombre = parseFloat(taux)

  // Calculer la nouvelle conversion
  const nouvelleConversion = eurNombre * tauxNombre
  

  
  console.log(nouvelleConversion)
  

  // Convertir le nouveau nombre en texte avec 2 chiffre après la virgule
  const nouvelleConversionTexte = nouvelleConversion.toFixed(2)

  console.log(nouvelleConversionTexte)

  // Mettre à jour le champ
  DOLeurInput.value = nouvelleConversionTexte
  DOLeurInput = math.round(DOLeurInput*100)
  DOLeurInput=(nouvelleConversionTexte/100)
})

DOLeurInput.addEventListener('input', () => {
  // la valeur saisie par l'utilisateur
  const DOL = DOLeurInput.value

  // Convertir la valeur en nombre
  const DOLNombre = parseFloat(DOL)

  // le taux
  const taux = DOLeurInput.getAttribute('data-taux')

  // Convertir le taux en nombre
  const tauxNombre = parseFloat(taux)

  // Calculer la nouvelle conversion
  const nouvelleConversion = DOLNombre / tauxNombre 
    

  console.log(nouvelleConversion)
  

  // Convertir le nouveau nombre en texte avec 2 chiffre après la virgule
  const nouvelleConversionTexte = nouvelleConversion.toFixed(2)

  console.log(nouvelleConversionTexte)

  // Mettre à jour le champ
  eurDOLInput.value = nouvelleConversionTexte
  eurDOLInput = math.round(eurDOLInput*100)
  eurDOLInput=(eurDOLInput/100)
})